import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_quiz/screens/quiz_screen.dart';
import 'package:get/route_manager.dart';
import 'package:websafe_svg/websafe_svg.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          WebsafeSvg.asset(
            "assets/bg.svg",
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
          SafeArea(
              child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: kDefaultFontSize),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Spacer(),
                Text(
                  "Let's Play Quiz,",
                  style: TextStyle(
                      fontSize: 36,
                      color: Colors.green[300],
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "Enter your Name",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black54,
                    fontWeight: FontWeight.w300,
                  ),
                ),
                TextField(
                  decoration: InputDecoration(
                      hintText: "Name....",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                      )),
                ),
                Spacer(),
                Container(
                  margin: EdgeInsets.all(16),
                  width: double.infinity,
                  child: ElevatedButton(
                      onPressed: () => Get.to(QuizScreen()),
                      child: Text('Start'),
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green[300],
                          textStyle:
                              TextStyle(color: Colors.white, fontSize: 26))),
                ),

                Spacer(),
                // ignore: prefer_const_constructors
              ],
            ),
          ))
        ],
      ),
    );
  }
}
