import 'package:flutter/material.dart';
import 'package:flutter_quiz/controllers/question_controller.dart';
import 'package:flutter_quiz/screens/quiz_screen.dart';
import 'package:flutter_quiz/screens/welcome_screen.dart';
import 'package:get/instance_manager.dart';
import 'package:get/route_manager.dart';
import 'package:websafe_svg/websafe_svg.dart';

class ScoreScreen extends StatelessWidget {
  const ScoreScreen({super.key});

  @override
  Widget build(BuildContext context) {
    QuestionController _qnController = Get.put(QuestionController());
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          WebsafeSvg.asset("assets/bg.svg", fit: BoxFit.fill),
          Column(
            children: [
              Spacer(
                flex: 3,
              ),
              Text(
                "Score",
                style: TextStyle(
                    fontSize: 36,
                    color: Colors.green[300],
                    fontWeight: FontWeight.bold),
              ),
              Spacer(),
              Text(
                "${_qnController.correctAns * 10}/${_qnController.questions.length * 10}",
                style: TextStyle(
                    fontSize: 36,
                    color: Colors.green[300],
                    fontWeight: FontWeight.bold),
              ),
              Spacer(
                flex: 3,
              ),
              Container(
                margin: EdgeInsets.all(16),
                width: double.infinity,
                child: ElevatedButton(
                    onPressed: () => Get.to(WelcomeScreen()),
                    child: Text('Start New Game'),
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.green[300],
                        textStyle:
                            TextStyle(color: Colors.white, fontSize: 26))),
              ),
              Spacer(
                flex: 3,
              ),
            ],
          )
        ],
      ),
    );
  }
}
