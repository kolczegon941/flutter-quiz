import 'package:flutter/material.dart';
import 'package:flutter_quiz/controllers/question_controller.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class ProgressBar extends StatelessWidget {
  const ProgressBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 35,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black45, width: 3),
        borderRadius: BorderRadius.circular(40),
      ),
      child: GetBuilder<QuestionController>(
        init: QuestionController(),
        builder: (controller) {
          return Stack(
            children: [
              LayoutBuilder(
                builder: (context, constraints) => Container(
                  //from 0.0 to 1.0 in 60s
                  width: constraints.maxWidth * controller.animation.value,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Colors.green,
                      Color.fromARGB(255, 29, 221, 163)
                    ]),
                    borderRadius: BorderRadius.circular(40),
                  ),
                ),
              ),
              Positioned.fill(
                  child: Row(
                children: [
                  Text(
                    "${(controller.animation.value * 60).round()} sec",
                    selectionColor: Colors.white12,
                  ),
                ],
              ))
            ],
          );
        },
      ),
    );
  }
}
